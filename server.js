const express = require('express');
const cors = require('cors');
const app = express();
app.use(cors());

const a = [0.0, 0.0, 0.0];

app.get('/up', (req, res) => {
  const b0 = parseFloat(req.query.a0);
  const b1 = parseFloat(req.query.a1);
  const b2 = parseFloat(req.query.a2);
  if (!(b0 > 0 && b1 > 0 && b2 > 0))
    res.sendStatus(400);
  else {
    a[0] = b0;
    a[1] = b1;
    a[2] = b2;
    res.sendStatus(200);
  }
});

app.get('/down', (req, res) => {
  res.json(a);
});

function getAzimuth(xloc, yloc) {
  const left = 100;
  const right = Math.sqrt(Math.pow(xloc, 2) + Math.pow(yloc, 2));
  const across = Math.sqrt(Math.pow(xloc, 2) + Math.pow(yloc - 10, 2));
  const theta = Math.acos((Math.pow(left, 2) + Math.pow(right, 2) - Math.pow(across, 2)) / (2 * left * right));

  if (xloc > 0) {
    return theta;
  } else if (xloc < 0) {
    return (2 * Math.PI) - theta;
  } else {
    return y >= 0 ? 0 : Math.PI;
  }

};

app.get('/azimuth', (req, res) => {
  const xcoor = (Math.pow(a[1], 2) - Math.pow(a[0], 2)) / 70;
  const ycoor = (2 * Math.pow(a[2], 2) - Math.pow(a[1], 2) - Math.pow(a[0], 2) + 12) / 40;

  res.json({
    x: xcoor,
    y: ycoor,
    azimuth: getAzimuth(xcoor, ycoor)
  });
});


const server = app.listen(process.env.PORT || 8080, () => {
  const host = server.address().address;
  const port = server.address().port;

  console.log(`Example app listening at http://${host}:${port}`);
});
